Feature: Finding a route
  Scenario: As an user I want to see the list of possible routes
    Given I press "Paraderos"
    When I wait for 1 second
    And I press "Rutas"
    Then I should see "Portal El Dorado"
  Scenario: As an user I want to see each stop from route 1
    Given I press "Rutas"
    When I wait for 1 second
    And I press "COMPLEMENTARIO"
    Then I should see "2-6"
    And I press "2-6"
    Then I should see "Ruta 2-6"
  Scenario: As an user I want to find the routes that go through calle 100
    Given I press "Paraderos"
    When I wait for 1 second
    And I press "AC 100"
    Then I should see "AC 100"

