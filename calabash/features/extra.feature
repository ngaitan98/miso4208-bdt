Feature: Extra features from the app
    Scenario: As an I user I want to see the news
        Given I wait for 1 second
        When I press "Paraderos"
        And I wait for 1 second
        And I scroll "down" until I see "Noticias"
        And I wait for 1 second
        And I press "Noticias"
        Then I should see "@BogotaTransito"
    Scenario: I want to go to  view the map
        Given I scroll "down" until I see "Mapa Sistema"
        When I press "Mapa Sistema"
        Then I wait for 1 second
    Scenario: I want to see the taxi fee per unity
        Given I wait for 5 seconds
        When I swipe left
        And I press "Taximetro"
        And I press "Detalle de recargos"
        And I wait for 1 second
        And I should see "Recargos"
        Then I press "Cerrar"

