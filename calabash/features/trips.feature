Feature: Planning a trip
    Scenario: As an user I want to plan a trip from my current location to Universidades
        Given I wait for 1 second
        When I press "Paraderos"
        And I wait for 1 second
        And I press "Planear viaje en Transmi o SITP"
        And I wait for 1 second
        And I press "Punto de destino"
        And I wait for 1 second
        And I press "Punto de destino"
        And I wait for 1 second
        And I press "Busca paradero de destino"
        And I enter "Universi" into input field number 1
        And I wait for 2 seconds
        And I press "Universidades"
        And I wait for 1 second
        Then I should see "Universidades"
    Scenario: As an user I want to plan a trip for ciclovia
        Given I wait for 1 second
        When I scroll "down" until I see "Ciclovía Dom/Fest"
        And I press "Ciclovía Dom/Fest"
        And I wait for 1 second
        And I scroll "down" until I see "Norte 3"
        And I press "Norte 3"
        Then I should see "Inicio"
    Scenario: As an user I want to see where is Cigarreria Cotrino to add money to my card
        Given I wait for 1 second
        When I scroll "down" until I see "Puntos de Recarga"
        And I press "Puntos de Recarga"
        And I wait for 1 second
        And I press "RECARGA"
        And I scroll "down" until I see "Cigarreria Cotrino"
        Then I press "Cigarreria Cotrino"

