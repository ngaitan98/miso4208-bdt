var { defineSupportCode } = require('cucumber');
var { expect } = require('chai');

defineSupportCode(({ Given, When, Then }) => {
    Given('I go to losestudiantes home screen', () => {
        browser.url('/');
        if (browser.isVisible('button=Cerrar')) {
            browser.click('button=Cerrar');
        }
    });

    When('I open the login screen', () => {
        setTimeout(()=>{
            browser.click('button=Ingresar');
        },3000)
    });

    When(/^I fill with (.*) and (.*)$/, (email, password) => {
        setTimeout(()=>{
            var cajaLogIn = browser.element('.cajaLogIn');
    
            var mailInput = cajaLogIn.element('input[name="correo"]');
            mailInput.click();
            mailInput.keys(email);
    
            var passwordInput = cajaLogIn.element('input[name="password"]');
            passwordInput.click();
            passwordInput.keys(password)
        },3000)
    });
    When('I try to login', () => {
        setTimeout(()=>{
            var cajaLogIn = browser.element('.cajaLogIn');
            cajaLogIn.element('button=Ingresar').click()
        },3000)
    });
    Then('I expect to see {string}', error => {
        setTimeout(()=>{
            var alertText = browser.element('.aviso.alert.alert-danger').getText();
            expect(alertText).to.include(error);
        },3000)
    });
    Then('I expect to login correctly', () => {
        setTimeout(()=>{
            browser.click('button[id="cuenta"]');
        },3000)
    });
});